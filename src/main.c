#include <stdio.h>
#include <stdlib.h>
#include "interface.h"

void __attribute__((constructor)) init(void) {
  setvbuf(stdout, NULL, _IONBF, 0);
  setvbuf(stdin, NULL, _IONBF, 0);
  setvbuf(stderr, NULL, _IONBF, 0);
}

int main(int argc, char **argv, char **env) {
  int choice = main_menu();
  switch (choice) {
  /* Register */
  case 1:
    break;
  /* Login */
  case 2:
    break;
  /* Exit */
  default:
    return EXIT_SUCCESS;
    break;
  }
  return EXIT_SUCCESS;
}
