#include "interface.h"

int main_menu(void) {
    char *banner = "| |     / /__  / /________  ____ ___  ___  \n| | /| / / _ \\/ / ___/ __ \\/ __ `__ \\/ _ \\\n| |/ |/ /  __/ / /__/ /_/ / / / / / /  __/\n|__/|__/\\___/_/\\___/\\____/_/ /_/ /_/\\___/ \n";
    char *mainmenu = "[1]: Login\n[2]: Register\n[3]: Exit\n";
    puts(banner);
    puts(mainmenu);
    int choice;
    scanf("%d", &choice);
    return 1;
}
