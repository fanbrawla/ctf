CC = gcc
CFLAGS = -O0 -R/home/pwn -L/home/pwn

.PHONY: build
build:
	$(CC) ./src/*.c $(CFLAGS) -o ./build/tsudoo
